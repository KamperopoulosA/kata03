public class Main {
    static String toCamelCase(String inputString){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(inputString);
        for(int i=0; i<stringBuilder.length();i++){
            char ch = stringBuilder.charAt(i);
            if (ch == '_') {
                stringBuilder.deleteCharAt(i);
                stringBuilder.setCharAt(i,Character.toUpperCase(stringBuilder.charAt(i)));
            }
        }
        return stringBuilder.toString();
    }
    static String toSnakeCase(String inputString){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(inputString);
        for(int i=0; i<stringBuilder.length();i++){
            char ch = stringBuilder.charAt(i);
            if (Character.isUpperCase(ch)) {
                stringBuilder.insert(i,'_');
                stringBuilder.setCharAt(i+1,Character.toLowerCase(stringBuilder.charAt(i+1)));
            }
        }
        return stringBuilder.toString();
    }

    public static void main(String[] args) {

        System.out.println(toCamelCase("hello_edabit"));
        System.out.println(toSnakeCase("getColor"));
        System.out.println(toCamelCase("is_modal_open"));
        System.out.println(toSnakeCase("helloEdabit"));

    }
}






    /*KATA: snake_case and camelCase
    Create two functions toCamelCase() and toSnakeCase() that each take a single
    string and convert it into either camelCase or snake_case.
            Examples
1. toCamelCase("hello_edabit") ➞ "helloEdabit"
            2. toSnakeCase("helloEdabit") ➞ "hello_edabit"
            3. toCamelCase("is_modal_open") ➞ "isModalOpen"
            4. toSnakeCase("getColor") ➞ "get_color"
    Notes
    Test input will always be appropriately formatted as either camelCase or
    snake_case, depending on the function being called*/


